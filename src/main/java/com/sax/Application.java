package com.sax;

import com.sax.configs.JpaConfig;
import com.sax.views.LoginView;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.swing.*;
import java.awt.*;

@SpringBootApplication
public class Application extends JFrame {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(JpaConfig.class);

        EventQueue.invokeLater(() -> {
            Application app = ctx.getBean(Application.class);
            app.setContentPane(ctx.getBean(LoginView.class));
            app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            app.pack();
            app.setLocationRelativeTo(null);
            app.setVisible(true);
        });
    }

}
