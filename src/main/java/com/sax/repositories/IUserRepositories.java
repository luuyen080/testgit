package com.sax.repositories;

import com.sax.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepositories extends JpaRepository<User, Integer> {
}
