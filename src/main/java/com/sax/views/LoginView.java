package com.sax.views;

import com.sax.Application;
import com.sax.services.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class LoginView extends JPanel {
    private JProgressBar progressBar1;
    private JPanel panel1;
    private JTable table1;
    private JButton button1;
    private Timer timer;
    private DefaultTableModel tableModel;

    @Autowired
    public LoginView(Application app, NhanVienView vienView, IUserServices userServices) {
        initComponent();
        tableModel = (DefaultTableModel) table1.getModel();
        button1.addActionListener((e) -> {
        });
        timer = new Timer(1, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                progressBar1.setValue(progressBar1.getValue() + 1);
                if (progressBar1.getValue() >= 1000) {
                    app.setContentPane(vienView);
                    app.pack();
                    app.setLocationRelativeTo(null);
                    timer.stop();
                }
            }
        });
        timer.start();

        tableModel.setDataVector(
                userServices.getAll().stream().map(i -> i.toObject()).toArray(Object[][]::new),
                new String[]{"id", "name"}
        );

    }

    private void initComponent() {

    }

    private void createUIComponents() {
        panel1 = this;
    }
}
