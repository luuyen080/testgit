package com.sax.services;

import com.sax.entities.User;

import java.util.List;

public interface ICrudServices<T, K> {
    List<User> getAll();
}
