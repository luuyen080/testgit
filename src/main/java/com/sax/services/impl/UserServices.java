package com.sax.services.impl;

import com.sax.entities.User;
import com.sax.repositories.IUserRepositories;
import com.sax.services.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServices implements IUserServices {
    @Autowired
    private IUserRepositories userRepository;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
}
